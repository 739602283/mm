package com.spring.aop.xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext ac = new ClassPathXmlApplicationContext("context.xml");
		Computer computer = (Computer) ac.getBean("computerImol");
		//System.out.println(computer.sub(2, 8));
		//System.out.println(computer.mul(2, 0));
		System.out.println(computer.div(2, 2));
	}

}
