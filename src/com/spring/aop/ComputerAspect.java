package com.spring.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 通知
 * 
 * @author Administrator
 *
 */
@Aspect
@Component
public class ComputerAspect implements Ordered {

	// 切入点表达式
	@Pointcut("execution(* *com.spring.aop.Computerimpl.*(..))")
	private void getExcution() {

	}

	/*
	 * * 通配符 代表所有 ..在方法的参数列表中使用 不限定类型 顺序 个数
	 */

	// 通知方法的参数当中 加入一个JoinPoint类型的数据即可
	@Before("getExcution()")
	public void a(JoinPoint jp) {

		// 通知方法的内部 如果想访问连接点的细节 如何完成？
		// JoinPoint:接口类型 连接点类型
		/*
		 * 如何获取方法的参数列表
		 */
		Object[] args = jp.getArgs();
		String methodName = jp.getSignature().getName();
		System.out.println("在调用" + methodName + "方法之前调用，参数为：" + Arrays.asList(args));
	}

	/**
	 * 
	 * @param jp
	 */
	@After("getExcution()")
	public void b(JoinPoint jp) {
		Object[] args = jp.getArgs();
		String methodName = jp.getSignature().getName();
		System.out.println("在调用" + methodName + "方法之后调用，参数为：" + Arrays.asList(args));
	}

	/**
	 * 返回值通知
	 * 
	 * 如何在返回值通知中获取方法执行之后的返回值
	 * 
	 * 1.需要在特定标注@AfterReturning(returning="变量名称") 作用：告知Spring要获取目标对象（被代理对象）方法的返回值
	 * result 2.在特定通知的参数列表中，指定一个形参名字和returning属性中的名字一致
	 * 
	 */
	@AfterReturning(pointcut = "execution(public int com.spring.aop.Computerimpl.*(..))", returning = "result")
	public void c(JoinPoint jp, Object result) {
		Object[] args = jp.getArgs();
		String methodName = jp.getSignature().getName();
		System.out.println("在调用" + methodName + "正常返回结果之后调用，参数为：" + Arrays.asList(args) + "正常返回结果为：" + result);
	}

	/**
	 * 异常通知
	 * 
	 * @param jp
	 * @param e
	 */
	@AfterThrowing(pointcut = "execution(public int com.spring.aop.Computerimpl.*(..))", throwing = "e")
	public void d(JoinPoint jp, Exception e) {
		Object[] args = jp.getArgs();
		String methodName = jp.getSignature().getName();
		System.out.println("出现了异常" + e.getMessage());
	}

	@Override
	public int getOrder() {
		// TODO Auto-generated method stub
		return 1;
	}

	/**
	 * 环绕通知 是代理的另外一种写法
	 * 
	 * 是否执行这个连接点
	 * 
	 * 环绕通知需要在通知方法的参数列表中，提供一个ProceedingJoinPoint子接口类型的形参
	 * 
	 * 
	 * @Around("execution(public int com.spring.aop.Computerimpl.*(..))") public
	 * void e(ProceedingJoinPoint joinPoint) {
	 * 
	 * //获取方法名称 String metyhodName=joinPoint.getSignature().getName() ;
	 * System.out.println("方法名称："+metyhodName);
	 * 
	 * try { // 前置通知 System.out.println("方法调用之前"); Object result=
	 * joinPoint.proceed(); // 返回值通知 System.out.println("正常返回一个结果 结果为："+result); }
	 * catch (Throwable e1) { // TODO Auto-generated catch block // 异常通知 //异常通知的内容
	 * e1来获取异常通知的实例 System.out.println("出现了异常 异常通知"); e1.printStackTrace(); }
	 * 
	 * // 后置通知 System.out.println("方法调用之后"); }
	 */
}
